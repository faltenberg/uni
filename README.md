#pragma compact_abi 

#include "UART0.h"
#include "support_common.h"  // include peripheral declarations and more
#include "uart_support.h"    // universal asynchronous receiver transmitter
#include "terminal_wrapper.h"

#include <stdio.h>
#include <stdlib.h>


// - Bitte darauf achten, dass am Coldfire-Serial Port ein  
//   Terminal (Putty o.�.) mit 19200kBaud angeschlossen ist.
// - Als Target muss   <projektname>_RAM_OSBDM   ausgew�hlt werden.


/**
 * Caller:
 * - save registers
 * - push args
 * - push return addr  \
 * - jump              / JSR
 * 
 * Callee:
 * - push frame pointer                 \
 * - set fp to sp                       | LINK
 * - allocate stack frame for variables /
 * - save registers
 * 
 * Callee:
 * - restore registers
 * - unwind stack frame (sp = fp)
 * - restore fp (UNLINK)
 * - return (RET)
 * 
 * Caller:
 * - rewind args
 * - restore registers
 */


void testPrintChar() {
	asm {
		BRA  start
		
		start:
		MOVE.B  #0x48, -(SP)
		JSR     TERM_Write
		ADDA.L  #1, SP
		
		// print "\r\n"
		MOVE.B  #0x0D, -(SP)
		JSR     TERM_Write
		MOVE.B  #0x0A, -(SP)
		JSR     TERM_Write
		ADDA.L  #2, SP
	}
}


void testPrintString() {
	asm {
		BRA  start
		
		str:
		DC.B	"Hello, world!"
		
		start:
		LEA		str, A1
		
		loop:
		MOVE.B	(A1)+, D0
		TST.B	D0
		BEQ		end
		MOVE.B  D0, -(SP)
		JSR     TERM_Write
		ADDA.L  #1, SP
		BRA		loop
		
		end:
		// print "\r\n"
		MOVE.B  #0x0D, -(SP)
		JSR     TERM_Write
		MOVE.B  #0x0A, -(SP)
		JSR     TERM_Write
		ADDA.L  #2, SP
	}
}


/**
 * Prints out a value in hex format. The function shifts the current nibble to print to the right and
 * uses bit manipulation to cast it to an ASCII character. Then the shift amount is reduced so the next
 * digit can be shifted to the right. Printing is performed beginning with the left-most digit.
 * 
 * Example:
 * 0. value = 0x12AB
 * 1. shift 1 by 12bits to the right -> value = 0x0001
 * 2. cast to ASCII and print
 * 3. shift 2 by 8bits to the right -> value = 0x0002
 * 4. cast to ASCII and print
 * 5. shift A by 4 bits to the right -> value = 0x000A
 * 6. cast to ASCII and print
 * 7. shift B by 0 bits to the right -> value = 0x000B
 * 8. cast to ASCII and print
 * 9. shift amount was 0, so we can stop looping back 
 * 
 * Register usage:
 * D0: current hex digit
 * D1: shift amount
 * D2: value to print
 */
void testPrintHex(int value) {
	asm { naked
		BRA  start
		
		start:
		// create stack frame
		LINK.W	A6, #0
		MOVE.L	D2, -(SP)
		MOVE.L	D1, -(SP)
		MOVE.L  D0, -(SP)
		
		// print "0x"
		MOVE.B  #0x30, -(SP)
		JSR     TERM_Write
		MOVE.B  #0x78, -(SP)
		JSR     TERM_Write
		ADDA.L  #2, SP

		MOVE.W	8(A6), D2  // get parameter (int is 2B)
		MOVE.L	#12, D1  // shift amount

		loop:
		// shift current digit to the right
		MOVE.L	D2, D0
		LSR.L	D1, D0
		AND.L	#0x000F, D0
		
		// if digit in 0..9: cast to ASCII '0'..'9'
		CMP.L	#10, D0
		BGE		_else
		OR.L	#0x0030, D0
		BRA		endif
		
		_else:
		// else digit in 10..15: cast to ASCII 'A'..'F'
		SUB.L	#9, D0
		OR.L	#0x0040, D0

		endif:
		// print character
		MOVE.L	D2, -(SP)
		MOVE.L	D1, -(SP)
		MOVE.B  D0, -(SP)
		JSR     TERM_Write
		ADDA.L  #1, SP
		MOVE.L	(SP)+, D1
		MOVE.L	(SP)+, D2
		
		// if not finished go back
		SUB.L	#4, D1
		TST.L	D1
		BGE		loop

		end:
		// print "\r\n"
		MOVE.B  #0x0D, -(SP)
		JSR     TERM_Write
		MOVE.B  #0x0A, -(SP)
		JSR     TERM_Write
		ADDA.L  #2, SP

		// unwind stack frame
		MOVE.L	(SP)+, D0
		MOVE.L	(SP)+, D1
		MOVE.L	(SP)+, D2
		UNLK	A6
		
		RTS
	}
}


/**
 * Executing the ILLEGAL instructions causes an exception. Execution is trapped into one of the
 * 256 exception vector traps. For ILLEGAL the trap code is at index 4 (offset thus is 16). With
 * the trap the faulty PC is pushed on the stack, then the status register. The vector resides at
 * address 0x20000000.
 */
void testIllegalCommand() {
	asm { naked
		BRA		start
		
		handler:
		MOVE.W	#0xDEAD, -(SP)
		JSR		testPrintHex
		ADDA.L	#2, SP
		
		// [SP+4] is the faulty PC, add to jump back to next instruction
		ADD.L	#2, 4(SP)
		RTE
		
		start:
		// register exception handler
		LEA		handler, A0
		MOVE.L	A0, 0x20000010
		
		ILLEGAL	//alternative: DC.W 0x4AFC
		
		RTS
	}
}


/**
 * Supervisor calls are performed with the TRAP instruction that traps into the
 * exception vector with offset 0x80. There are 16 handlers available. The next
 * PC and the status register are pushed on the stack during TRAP.
 */
void testSupervisorCall() {
	asm { naked
		BRA		start
		stack:	dc.b "\0\0\0\0\0\0\0\0\0\0"
		
		// print address of user stack passed as parameter
		handler:
		MOVE.L	A0, -(SP)  // save registers
		MOVE.L	USP, A0  // get user stack pointer

		MOVE.W	2(A0), -(SP)
		JSR		testPrintHex
		ADDA.L	#2, SP
		MOVE.W	(A0), -(SP)
		JSR		testPrintHex
		ADDA.L	#2, SP
		
		// clean up user stack
		ADDA.L	#2, A0
		MOVE.L	A0, USP
		
		// restore registers and return
		MOVE.L	(SP)+, A0
		RTE
		
		start:
		// register supervisor handler at index 1 (thus offset 0x80 + 0x04)
		LEA		handler, A0  
		MOVE.L	A0, 0x20000084
		
		// print supervisor stack pointer
		MOVE.L	SP, -(SP)  // push upper half
		ADDA.L	#2, SP
		MOVE.W	SP, -(SP)  // push lower half
		JSR		testPrintHex
		ADDA.L	#2, SP
		JSR		testPrintHex
		ADDA.L	#2, SP
		
		// init user stack, switch to user mode
		LEA		stack+10, A0
		MOVE.L	A0, USP
		MOVE.W	SR, D0
		ANDI.L	#0xdfff, D0  // SR[S]=0 and thus user mode
		MOVE.W	D0, SR
		
		// perform a supervisor call
		MOVE.L	SP, -(SP)
		TRAP	#1
		HALT

		RTS
	}
}


void testExecutePrivilegedInstruction() {
	asm {
		BRA		start
		
		handler:
		MOVE.W	#0xDEAD, -(SP)
		JSR		testPrintHex
		ADDA.L	#2, SP
		
		// [SP+4] is the faulty PC, add to jump back to next instruction
		ADD.L	#2, 4(SP)
		RTE
		
		start:
		// register exception handler for privilege violation
		LEA		handler, A0
		MOVE.L	A0, 0x20000020
		
		// switch to user mode
		MOVE.W	SR, D0
		ANDI.L	#0xdfff, D0  // SR[S]=0 and thus user mode
		MOVE.W	D0, SR
		
		// perform a privileged instruction
		HALT
		
		MOVE.W	#0xBEEF, -(SP)
		JSR		testPrintHex
		ADDA.L	#2, SP
		
		RTS
	}
}

// define GPIO ports
#define GPIO_PORTNQ        0x40100008
#define GPIO_DDRNQ         0x40100020
#define GPIO_PORTNQP       0x40100038
#define GPIO_CLRNQ         0x40100050
#define GPIO_PNQPAR	       0x40100068

#define GPIO_PORTTC        0x4010000F
#define GPIO_DDRTC         0x40100027
#define GPIO_SETTC         0x4010003F
#define GPIO_CLRTC         0x40100057
#define GPIO_PTCPAR        0x4010006F

/**
 * SW1 on IRQ5/FEC_MDC/PNQ5			-> Port NQ5  (bit 5, par 11.10)
 * SW2 on IRQ1/USB_ALTCLK/PNQ1		-> Port NQ1  (bit 1, par 3..2)
 * LED1 on DTIN0/DTOUT0/PWM0/PTC0	-> Port TC0  (bit 0, par 1..0)
 * LED2 on DTIN1/DTOUT1/PWM2/PTC1	-> Port TC1  (bit 1, par 3..2)
 * LED3 on DTIN2/DTOUT2/PWM4/PTC2	-> Port TC2  (bit 2, par 5..4)
 * LED4 on DTIN3/DTOUT3/PWM6/PTC3	-> Port TC3  (bit 3, par 7..6)
 * 
 * GPIO pins are grouped to 8bit ports (not all ports use all 8 bits). Each port has registers to
 * configure the port pins.
 * 
 * GPIO_PORTxx: Port Output Data Register
 * - store data to drive the port pins (if they are configured for output)
 * - reading gives the value in register, not the data from port pins!
 * 
 * GPIO_DDRxx: Port Data Direction Register
 * - defines whether a port pin is input (bit cleared) or output (bit set)
 * 
 * GPIO_PORTxxP: Port Pin Data
 * GPIO_SETxx: Set Data Register
 * - reading gives the current port pin state
 * - writing a 1 sets the bit in the GPIO_PORTxx register
 *  
 * GPIO_CLRxx: Clear Output Data Register
 * - writing a 0 clears the bit in the GPIO_PORTxx register
 * 
 * GPIO_PxxPAR: Port Pin Assignment Register
 * - uses 2 bits per port
 * - writing 00 sets pin for GPIO, writing 01 sets to primary function (interrupts)
 * - port NQ is reseted for interrupts (IRQ1 and IRQ5), all other ports for GPIO
 */

void testPowerOnLEDWhileButtonPressed() {
	asm { naked
		BRA		start
		
		start:
		LINK.W	A6, #0
		MOVE.L	D0, -(SP)
		MOVE.L	D1, -(SP)
		
		// init LEDs for GPIO output
		MOVE.B	#0, D0
		MOVE.B	D0, (GPIO_PTCPAR)
		MOVE.B	#0xf, D0
		MOVE.B	D0, (GPIO_DDRTC)
		
		// init buttons for GPIO input
		MOVE.W	(GPIO_PNQPAR), D0
		ANDI.L	#0xF3F3, D0
		MOVE.W	D0, (GPIO_PNQPAR)
		MOVE.B	(GPIO_DDRNQ), D0
		ANDI.L	#0xD2, D0
		MOVE.B	D0, (GPIO_DDRNQ)
		
		// switch off LEDs
		MOVE.B  #0, D0
		MOVE.B	D0, (GPIO_CLRTC)
		
		loop:
		// read buttons (0: pressed, 1: released) and switch on/off LEDs (0: off, 1: on)
		EOR.L	D0, D0
		EOR.L	D1, D1
		MOVE.B	(GPIO_PORTNQP), D1
		NOT.L	D1
		ANDI.L	#0x22, D1
		OR.L	D1, D0
		LSR.L	#5, D1
		OR.L	D1, D0
		MOVE.B	D0, (GPIO_PORTTC)
		BRA		loop

		MOVE.L	(SP)+, D1
		MOVE.L	(SP)+, D0
		UNLK	A6
		RTS
	}
}


// 0x1234ABCD = 0001 0010 0011 0100 1010 1011 1100 1101
// 0xB3D52C48 = 1011 0011 1101 0101 0010 1100 0100 1000
inline void testBitrev() {
	asm { naked
		BRA  start
		num:	dc.l	0x1234ABCD
		
		start:
		// store registers
		SUBA.L	#8, SP
		MOVEM.L	D1/A1, (SP)
		
		LEA		num, A1
		MOVE.L	(A1), -(SP)
		JSR		testPrintHex
		ADDA.L	#2, SP
		JSR		testPrintHex
		ADDA.L	#2, SP
		
		MOVE.L	(A1), D1
		BITREV.L	D1
		
		MOVE.L	D1, -(SP)
		JSR		testPrintHex
		ADDA.L	#2, SP
		JSR		testPrintHex
		ADDA.L	#2, SP

		// restore registers
		MOVEM.L	(SP), D1/A1
		ADDA.L	#8, SP
	}
}

/**
 * malloc() expects argument in D0 and returns result in A0
 * A2 points to current element (or NULL if end of list)
 * A3 points to the pointer of prev element
 *
 * anchor                A3          A2
 *    |                  |           |
 * ___V___     __________V___     ___V__________     _____________     
 * |     | --> | num | next | --> | num | next | --> | num |  0  |
 * -------     --------------     --------------     -------------
 * 
 * Works with:
 * - list end: A3 points to NULL pointer, A2 is NULL
 * - empty list: A3 points to anchor, A2 and anchor are NULL
 * - insert before first element: A3 points to anchor, A2 points to first element
 */
void testLinkedList() {
	asm { naked
		BRA  start
		anchor:	dc.l	0x00000000
		
		start:
		// store registers
		SUBA.L	#64, SP
		MOVEM.L	D0-D3/A0-A3, (SP)
		
		// init
		MOVE.W	#42, -(SP)
		PEA		_1
		BRA		_insertNum
		_1:
		ADD.L	#2, SP
		
		MOVE.W	#44, -(SP)
		PEA		_2
		BRA		_insertNum
		_2:
		ADD.L	#2, SP
		
		MOVE.W	#41, -(SP)
		PEA		_3
		BRA		_insertNum
		_3:
		ADD.L	#2, SP
		
		MOVE.W	#43, -(SP)
		PEA		_4
		BRA		_insertNum
		_4:
		ADD.L	#2, SP
		
		PEA		_P
		BRA		_printList
		_P:
		
		// restore registers
		MOVEM.L	(SP), D0-D3/A0-A3
		ADDA.L	#64, SP
		RTS
		
		_insertNum:
		// create new node
		MOVE.L	#6, D0        // allocate 6B
		JSR		malloc        // address in A0
		TST.L	A0
		BEQ		_error
		MOVE.W	-4(SP), (A0)  // store number in node
		CLR.L	2(A0)         // A0 -> | num | NULL |
		
		LEA		anchor, A3
		MOVE.L	(A3), A2
		_loopIN:
		TST.L	(A2)          // if list end reached
		BEQ		_endIN        // exit loop and insert node at A3
		
		// test current node with new item
		MOVE.L	(A2), D0
		EXT.L	D0
		MOVE.L	-4(SP), D1
		EXT.L	D1
		CMP.L	D0, D1		  // break if new < old <=> new - old < 0
		BLT		_endIN		
		
		// continue
		LEA		2(A2), A3	  // A3 = A2+2
		MOVE.L	(A3), A2      // A2 = [A2+2]
		BRA		_loopIN
		
		_endIN:
		MOVE.L	A0, (A3)      // insert new node
		MOVE.L	A2, 2(A0)     // link to next node if there is one
		_error:
		RTS
		
		_printList:
		LEA		anchor, A2
		_loopPL:
		MOVE.L	(A2), A2
		TST.L	A2
		BEQ		_endPL
		MOVE.L	(A2), -(SP)
		JSR		testPrintHex
		ADDA.L	#2, SP
		MOVE.L	2(A2), A2
		BRA		_loopPL
		
		_endPL:
		MOVE.W	A2, -(SP)
		JSR		testPrintHex
		ADDA.L	#2, SP
		RTS
	}
}
// ret  <- sp
// arg


void main(void) {
	// Declarations MUST be made here first, otherwise compile error
	int value = 0xAB12;
	
	// Processor Expert internal initialization. DON'T REMOVE THIS CODE!!!
	PE_low_level_init();
	// TODO: switch to user mode

//	testPrintChar();
//	testPrintString();
//	testIllegalCommand();
//	testSupervisorCall();
//	testPrintHex(value);
//	testPowerOnLEDWhileButtonPressed();
//	testExecutePrivilegedInstruction();
//	testBitrev();
	testLinkedList();
	
	// Als Ende-Behandlung nachfolgend ein einfacher Leerlauf-Prozess
	for (;;) {
	}
}
